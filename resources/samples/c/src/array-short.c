
#include "test.h"

int _a[6] = { 2, 3, 5, 7, 9, 11};

int array_SHORT (short* a, size_t sz)
{
    int i, r;

    r = 0;
    for ( i = 1; i < sz; i++) {
        r += a [i] - a [i-1];
    }

    return (r);
}

int array_INT (int*, size_t) __attribute__ ((noinline));
int array_INT (int* a, size_t sz)
{
    int i, r;

    r = 0;
    for ( i = 1; i < sz; i++) {
        r += a [i] - a [i-1];
    }

    return (r);
}

int array_INT_CALL ()
{
    int a[6] = {2, 3, 5, 7, 9, 11};

    return ( array_INT (a, 6));
}

int array_INT_STACK ()
{
    int a[6] = {2, 3, 5, 7, 9, 11};
    int i, r;

    r = 0;
    for (i = 1; i < 6; i++) {
        r += a[i] - a[i-1];
    }

    return (r);
}

int array_INT_GLOBAL ()
{
   int i,r;

   r = 0;
   for (i = 1; i < 6; i++) {
      r += _a[i] - _a[i-1];
   }

   return (r);
}

int array_INT_GLOBAL_CALL ()
{
   return (array_INT (_a, 6));
}
