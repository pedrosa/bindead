//@summarystart
// Example in which McVeto avoids exhaustive loop unrolling
// by discovering the right "candidate" loop invariants.
//@summaryend
#include "../include/reach.h"

#define N 500
#define M 200

int MakeChoice() { return 0; }

int main()
{
  int x, y, z;

  if(z == 0)
  {
    x = M;
    y = N - M;
    while(x > 0) {
      x--;
      y++;
    }
  }
  else 
  {
    x = N - M;
    y = M;
    while(x > 0) {
      x--;
      y++;
    }
  }

  if (y != N)
    UNREACHABLE(); 

  return 0;
}

