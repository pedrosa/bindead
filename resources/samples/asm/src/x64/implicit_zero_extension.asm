
format elf64
	
section '.data' writable

foo		dw 0x2342
msg		db 'Hello World64!', 0x0A
msg_size	= $-msg

section '.text' executable

public main

main:
   mov   rax, -1
   mov   eax, 0

   ; exit
   xor     edi, edi
   mov     eax, 60
   syscall
