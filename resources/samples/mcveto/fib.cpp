//@summarystart
// Recursive program which verifies that the fourth
// Fibonacci number is 4.
//@summaryend
#include "../include/reach.h"

#define DEFAULT_INT_BITS 5

int fib(int n) {
  
  int n_1, n_2;
	
  if(n == 0)
    return 0;
  if(n == 1)
    return 1;
  else {
    n_1 = fib(n-1);
    n_2 = fib(n-2);
    return n_1 + n_2;
  }
}

int main() {
	
  int r, x;
  
  r = fib(4);
  
  if(r == 3) {
    x=7;
#if REACH==1
    REACHABLE();
#endif
  }
  else
  {
    x=77;
#if REACH==0
    UNREACHABLE();
#endif
  }
  
  return 0;
}
